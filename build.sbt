name := "play-sass"

version := "1.0-SNAPSHOT"

sbtPlugin := true

organization := "net.caffeinelab"

description := "Sass assets plugin for Play 2.1"

libraryDependencies += "org.jruby" % "jruby-complete" % "1.7.1"

addSbtPlugin("play" %% "sbt-plugin" % "2.1-RC1")

resolvers += "Typesafe repository releases" at "http://repo.typesafe.com/typesafe/releases/"

resolvers += "Typesafe repository snapshots" at "http://repo.typesafe.com/typesafe/snapshots/"

resolvers += Resolver.file("play-local", file("/Volumes/Data/code/play-2.1-RC1/repository/local/"))(Resolver.ivyStylePatterns)

// resolvers += "play-local" at "file:///Volumes/Data/code/play-2.1-RC1/repository/local/"